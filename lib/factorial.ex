defmodule Factorial do
  def factorial(0), do: 1
  def factorial(n), do: n * factorial(n - 1)
end

IO.inspect(Factorial.factorial(0))
IO.inspect(Factorial.factorial(1))
IO.inspect(Factorial.factorial(4))
IO.inspect(Factorial.factorial(7))
IO.inspect(Factorial.factorial(17))